FROM        illinisat2-u-boot-base:latest    AS u-boot
FROM        illinisat2-linux-base:latest     AS linux

FROM        illinisat2-buildroot-base:latest AS buildroot
USER        build-user
WORKDIR     /home/build-user
COPY        --from=u-boot /home/build-user/u-boot/u-boot /home/build-user/u-boot/u-boot.img /home/build-user/u-boot/MLO ./
COPY        --from=linux /home/build-user/linux/rootfs/lib /home/build-user/linux/arch/arm/boot/zImage /home/build-user/linux/arch/arm/boot/dts/am335x-mitysom-illinisat2.dtb ./
RUN         mkdir post-build
COPY        post-build ./post-build
RUN         mkdir output && \
            mkdir sd1 && \
            mkdir sd2 && \
            mkdir sd3 && \
            cp am335x-mitysom-illinisat2.dtb ./sd1/ && \
            cp MLO ./sd1/ && \
            cp u-boot.img ./sd1/ && \
            cp post-build/uEnv.txt ./sd1/ && \
            cp zImage ./sd1/ && \
            tar xf ./buildroot-2019.08/output/images/rootfs.tar -C ./sd2 && \
            cp -R post-build/rootfs/* ./sd2/ && \
            cp -R modules ./sd2/lib/ && \
            chmod +x ./sd2/flash.sh && \
            chmod -x ./sd2/etc/init.d/S93-am335x-pm-firmware-load && \
            chmod -x ./sd2/etc/init.d/S99at && \
            chmod +x ./sd2/etc/init.d/S98modprobe && \
            chmod +x ./sd2/etc/init.d/S97stty && \
            cd ./sd3/ && \
            cp ../post-build/ubinize.cfg ./ && \
            mkfs.ubifs -r ../sd2/ -F -o ubifs.img -m 4096 -e 253952 -c 1580 && \
            ubinize -o ubi.img -m 4096 -p 256KiB -s 4096 -O 4096 ubinize.cfg && \
            cd .. && \
            tar czf output/files.tar.gz ./sd1 ./sd2 ./sd3
ENTRYPOINT  ["/usr/bin/bash"]
