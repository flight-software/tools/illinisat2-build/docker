#!/bin/zsh                                                                            

set -ex 

mkdir -p /mnt/card
mkdir -p /mnt/sd1
mkdir -p /mnt/sd3

mount /dev/mmcblk0p1 /mnt/sd1
mount /dev/mmcblk0p3 /mnt/sd3

cd /mnt/sd1

flash_erase /dev/mtd1 0 0 
nandwrite -p /dev/mtd1 MLO 
flash_erase /dev/mtd2 0 0 
nandwrite -p /dev/mtd2 MLO 
flash_erase /dev/mtd3 0 0 
nandwrite -p /dev/mtd4 MLO 
flash_erase /dev/mtd4 0 0 
nandwrite -p /dev/mtd4 MLO 
flash_erase /dev/mtd5 0 0 
nandwrite -p /dev/mtd5 u-boot.img
flash_erase /dev/mtd6 0 0 
nandwrite -p /dev/mtd6 uEnv.txt
flash_erase /dev/mtd7 0 0 
nandwrite -p /dev/mtd7 zImage
flash_erase /dev/mtd8 0 0 
nandwrite -p /dev/mtd8 am335x-mitysom-illinisat2.dtb

cd /mnt/sd3

ubiformat /dev/mtd9 -f ubi.img -s 4096 -O 4096
ubiattach -m 9 -O 4096
mount -t ubifs ubi0_0 /mnt/card
sync

cd /mnt/sd1
sync

cd /mnt/card
sync

