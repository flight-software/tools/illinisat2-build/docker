#!/bin/bash
set -ex
sudo /bin/bash -c '
set -ex
docker build -t illinisat2-arch-base:latest       arch-base
docker build -t illinisat2-crosstools-base:latest crosstools-base &
docker build -t illinisat2-mdk-base:latest        mdk-base &
wait
docker build -t illinisat2-buildroot-base:latest  buildroot-base &
docker build -t illinisat2-u-boot-base:latest     u-boot-base &
wait `echo $!`
docker build -t illinisat2-linux-base:latest      linux-base
wait
docker build -t illinisat2:latest .'

