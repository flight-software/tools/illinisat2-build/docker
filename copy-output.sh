#!/bin/bash
export ME=$(whoami)
export GR=$ME
set -ex
sudo /bin/bash -c '
set -ex
rm -f files.tar.gz
echo "exit" | docker run -i illinisat2:latest
docker cp $(sudo docker ps -alq):/home/build-user/output/files.tar.gz files.tar.gz
chown "$ME":"$GR" files.tar.gz'

